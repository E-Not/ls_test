import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../pages/Main.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
  },
  {
    path: '/library/:id',
    name: 'Detail',
    component: () => import(/* webpackChunkName: "detail" */ '@/pages/Detail.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
