import axios from 'axios';

const STATE = {
  libraries: [],
  isLoading: false,
};

const ACTIONS = {
  fetchLibraries: async ({ commit }) => {
    commit('toggleLoading');
    try {
      const response = await axios.get('/data/data.json');
      if (response.status === 200) {
        commit('setLibraries', response.data);
        commit('toggleLoading');
      }
    } catch (e) {
      commit('toggleLoading');
    }
  },
  getLibraryById: async ({ state, dispatch }, id) => {
    if (state.libraries.length === 0) {
      await dispatch('fetchLibraries');
    }
    return state.libraries.find((el) => el._id === id);
  },
};

const GETTERS = {
  loading: (state) => state.isLoading,
  allLibraries: (state) => state.libraries,
  librariesFlatArray: (state) => state.libraries.map((item) => {
    const id = item._id;
    const origin = item.data.general;
    const name = origin.name || '';
    const description = origin.description || '';
    const address = origin.address.fullAddress || '';
    const organization = origin.organization.name || '';
    return {
      id,
      name,
      description,
      address,
      organization,
      origin,
    };
  }),
};

const MUTATIONS = {
  toggleLoading: (state) => {
    state.isLoading = !state.isLoading;
  },
  setLibraries: (state, libs) => {
    state.libraries = libs;
  },
};

export default {
  namespaced: true,
  state: STATE,
  getters: GETTERS,
  actions: ACTIONS,
  mutations: MUTATIONS,
};
